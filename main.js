function setInputFilter(textbox, inputFilter) {
  [
    "input",
    "keydown",
    "keyup",
    "mousedown",
    "mouseup",
    "select",
    "contextmenu",
    "drop",
  ].forEach(function (event) {
    if (textbox) {
      textbox.addEventListener(event, function () {
        if (inputFilter(this.value)) {
          this.oldValue = this.value;
          this.oldSelectionStart = this.selectionStart;
          this.oldSelectionEnd = this.selectionEnd;
        } else if (this.hasOwnProperty("oldValue")) {
          this.value = this.oldValue;
          this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
        } else {
          this.value = "";
        }
      });
    }
  });
}
$(document).on("keyup", function () {
  console.log(document.querySelectorAll(".onlyNumber"));
  document.querySelectorAll(".onlyNumber").forEach((ele) => {
    setInputFilter(ele, function (value) {
      return /^-?\d*$/.test(value);
    });
  });
});
var firebaseConfig = {
  apiKey: "AIzaSyB_U8UiLqRA1zZgnrksCW4dqVhiM4bWxCg",
  authDomain: "agavan-trust.firebaseapp.com",
  databaseURL: "https://agavan-trust-default-rtdb.firebaseio.com",
  projectId: "agavan-trust",
  storageBucket: "agavan-trust.appspot.com",
  messagingSenderId: "887938411721",
  appId: "1:887938411721:web:c8c338cb39eaa70c2f7118",
  measurementId: "G-DMX69RN7RV",
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();

var jbox = "";

function razorpayment(data) {
  var options = {
    key: "rzp_live_bf6xdn41dIoK8r",
    amount: data.amount * 100, // 2000 paise = INR 20
    name: "Agavan",
    description: "Razoarpay Payment",
    image: "",
    notes: {
      name: data.name,
      email: data.monumber,
      mobile: data.email,
      amount: data.amount,
    },
    handler: function (response) {
      capturePayment(response.razorpay_payment_id, data);
    },
    prefill: {},
    theme: {},
    modal: {
      ondismiss: function () {},
      escape: false,
    },
  };
  var rzp1 = new Razorpay(options);
  rzp1.open();
}

function capturePayment(id, data) {
  $.ajax({
    type: "post",
    url: "payment.php",
    data: { data: { amount: data.amount * 100, currency: "INR" }, payment_id: id },
    success: function (response) {
      response = JSON.parse(response)
      console.log(response)
      firebase
        .database()
        .ref("donation/" + response.id)
        .set({
          name: data.name,
          email: data.email,
          mobile: data.monumber,
          amount: data.amount,
          payment_id: response.id,
        });
      myModal.close();
      successModel = new jBox("Modal", {
        width:300,
        height:300,          
        animation: "zoomOut",
        content: `<div class="center">
                    <div class="success-checkmark">
                    <div class="check-icon">
                      <span class="icon-line line-tip"></span>
                      <span class="icon-line line-long"></span>
                      <div class="icon-circle"></div>
                      <div class="icon-fix"></div>
                    </div>
                    </div>
                    <p>donated successfully</p>
                  </div>`,
      });
      successModel.open();
      setTimeout(function(){
        successModel.close();
      },3000)
    },
    
  });
}
var successModel = '';


$(document).on("click", ".donate-btn", function () {
  var source = $(this).attr( 'data-source' );
  var id = $.now();
  var name = $("#"+source+" h3").text();
  var image = $("#"+source+"  img").attr("src");;
  var disc = $("#"+source+" p").text();;
  var html = `<div class="main-row">
                <div class="halfDiv">
                  <img src="${image}" />
                  <h4>${disc}</h4>
                </div>
                <div class="halfDiv">
                <form id="donationForm">
                  <div>
                    <label> Name </label> 
                    <input type="text" class="form-control" id="name" name="name" >
                    <span class="error" id="name_er"> </span>
                  </div>
                  <div>
                    <label> Mobile number </label> 
                    <input type="text" max=10 class="onlyNumber form-control" id="monumber" name="monumber" maxlength="10" >
                    <span class="error" id="monumber_er"> </span>
                  </div>
                  <div>
                    <label> Email </label> 
                    <input type="text" id="email" class="form-control" name="email" >
                    <span class="error" id="email_er"> </span>
                  </div>
                  <div>
                    <label> Amount </label> 
                    <input type="text" class="onlyNumber form-control mb-2" id="amount" name="amount" >
                    <span class="error" id="amount_er"> </span>
                  </div>
                  <div>
                    <label> Address </label> 
                    <textarea class="form-control mb-2" name="address" id="address"></textarea>
                    <span class="error" id="address_er"> </span>
                  </div>
                  <div id="recaptcha_${id}" class="captcha-contact"></div> 
                  <div>
                  
                    <input type="submit" class="u-border-2 u-border-black u-btn u-button-style u-hover-custom-color-2 u-none u-text-black u-text-hover-black u-btn-1" value="Donate">
                  </div>
                </form>
                </div>
            </div>`;
          myModal = new jBox("Modal", {
            title: name,
            animation: "zoomOut",
            maxWidth: 1000,
            content: html,
              
          });
        
          myModal.open();

          grecaptcha.render('recaptcha_'+id, {
            'sitekey' : '6Lcs_okaAAAAAEHr4RIv5re7mKt2w_snqpjvi0k_'
          });
});


$(document).on("submit", "#donationForm", function (e) {
  e.preventDefault();
  // onloadCallback();
  console.log("recaptcha", grecaptcha.getResponse());
  grecaptcha.reset();
  console.log("reset captcha", grecaptcha.getResponse());
  $(".error").text("");
  var isRetune = false;
  if ($("#name").val() == "") {
    $("#name").addClass("error_border");
      $("#name").keyup(function(){
        $("#name").removeClass("error_border");
        });
    isRetune = true;
  }
  if ($("#monumber").val() == "") {
    $("#monumber").addClass("error_border");
      $("#monumber").keyup(function(){
        $("#monumber").removeClass("error_border");
        });
    isRetune = true;
  } else {
    if (!CheckIndianNumber($("#monumber").val())) {
      $("#monumber_er").text("Enter valid mobile number");
      $("#monumber").keyup(function(){
        $("#monumber_er").text("");
        });
      isRetune = true;
    }
  }
  if ($("#email").val() == "") {
    $("#email").addClass("error_border");
    $("#email").keyup(function(){
      $("#email").removeClass("error_border");
      });
    isRetune = true;
  } else {
    if (!ValidateEmail($("#email").val())) {
      $("#email_er").text("Enter valid email");
      $("#email").keyup(function(){
        $("#email_er").text("");
        });
      isRetune = true;
    }
  }

  if ($("#amount").val() == "") {
    $("#amount").addClass("error_border");
    $("#amount").keyup(function(){
      $("#amount").removeClass("error_border");
      });
    isRetune = true;
  }
  if ($("#address").val() == "") {
    $("#address").addClass("error_border");
    $("#address").keyup(function(){
      $("#address").removeClass("error_border");
      });
    isRetune = true;
  }
  if (isRetune) return false;
  var data = {};
  data.name = $("#name").val();
  data.monumber = $("#monumber").val();
  data.email = $("#email").val();
  data.amount = $("#amount").val();
  data.address = $("#address").val();
  razorpayment(data);
});
function ValidateEmail(mail) {
  if (
    /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(
      mail
    )
  ) {
    return true;
  }
  return false;
}
function CheckIndianNumber(b) {
  var a = /^\d{10}$/;
  if (a.test(b)) {
    return true;
  } else {
    return false;
  }
}
